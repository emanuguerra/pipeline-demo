# language: en
Feature: Fill the water tank

  @Machine @Tank
  Scenario:

    The coffee machine is equiped with a water tank that needs to be refilled when empty.

    Given a coffee machine with an empty tank
    When I fill the water tank
    Then the machine water tank should not be empty

