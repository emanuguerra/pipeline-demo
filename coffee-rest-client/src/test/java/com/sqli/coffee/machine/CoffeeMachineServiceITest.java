package com.sqli.coffee.machine;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by ema on 01/06/16.
 */
public class CoffeeMachineServiceITest {

    @Given("^a coffee machine with an empty tank$")
    public void a_coffee_machine_with_an_empty_tank() {

    }

    @When("^I fill the water tank$")
    public void i_fill_the_water_tank() {

    }

    @Then("^the machine water tank should not be empty$")
    public void the_machine_water_tank_should_not_be_empty() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }

}
