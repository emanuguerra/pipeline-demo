package com.sqli.coffee.machine;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by ema on 08/06/16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = false,
        strict = true,
        features = "src/test/resources",
        format = {
                "pretty",
                "html:target/cucumber",
                "json:target/json/cucumber.json",
                "junit:target/junit/cucumber.xml"
        }
)
public class CukesITests {

}
