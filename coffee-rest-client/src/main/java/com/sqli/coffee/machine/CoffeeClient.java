package com.sqli.coffee.machine;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ema on 01/06/16.
 */
@FeignClient(url = "${feign.url}")
public interface CoffeeClient {

    @RequestMapping(value = "/tank", method = RequestMethod.PUT)
    void fillTank();
}
