node {

    def mvnHome = tool 'M3'

    stage concurrency: 3, name: 'Checkout project'
    echo 'Checking out project ....'
    checkout scm

    stage concurrency: 3, name: 'Build project'
    try {

        def v = version()
        if (v) {
            echo "Building version ${v}"
        }

        sh "${mvnHome}/bin/mvn clean install"
        step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])

    } catch (e) {
        currentBuild.result = 'FAILURE'
        e.printStackTrace()
        throw e
    } finally {
        processStageResult()
    }

    stage concurrency: 1, name: 'Deploy application'
    try {

        echo 'Starting service container ...'

        sh 'docker stop coffee-micro-service 2>/dev/null'
        sh 'docker rm coffee-micro-service 2>/dev/null'
        sh 'docker run -d --name coffee-micro-service -p 8082:8080 -t sqli/demo-service'
        step([$class: 'ArtifactArchiver', artifacts: '**/target/*.jar'])

    } catch (e) {
        currentBuild.result = 'FAILURE'
        e.printStackTrace()
        throw e
    } finally {
        processStageResult()
    }

    stage concurrency: 1, name: 'Acceptance tests'
    try {

        echo 'Starting acceptance tests ...'

        sh "cd coffee-rest-client && ${mvnHome}/bin/mvn clean verify"
        step([$class: 'CucumberReportPublisher', fileExcludePattern: '', fileIncludePattern: '**/cucumber.json', ignoreFailedTests: false, jenkinsBasePath: '', jsonReportDirectory: '', missingFails: false, parallelTesting: false, pendingFails: false, skippedFails: false, undefinedFails: false])

    } catch (e) {
        currentBuild.result = 'FAILURE'
        throw e
    }
    finally {
        processStageResult()
    }
}


def version() {
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    matcher ? matcher[0][1] : null
}

def processStageResult() {
//    step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: "${EMAIL}", sendToIndividuals: false])

    if (currentBuild.result != null && currentBuild.result != "SUCCESS") {
        sh "exit 1"
    }
}