import com.sqli.web.api.coffee.CoffeeMachine;
import com.sqli.web.api.coffee.CoffeeMachineService;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ema on 01/06/16.
 */
public class CoffeeMachineTest {

    @Test
    public void testFillWaterTank() {
        CoffeeMachine coffeeMachine = new CoffeeMachine();
        coffeeMachine.fillTank();
        Assert.assertFalse(coffeeMachine.isEmptyTank());
    }

    @Test
    public void testEmptyWaterTank() {
        CoffeeMachine coffeeMachine = new CoffeeMachine();
        coffeeMachine.emptyTank();
        Assert.assertTrue(coffeeMachine.isEmptyTank());
    }
}
