package com.sqli.web.api.coffee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoffeeMachineService {

    @Autowired
    CoffeeMachine coffeeMachine;

    @RequestMapping(value = "/tank", method = RequestMethod.PUT, params = "fill")
    public void fillTank(@RequestParam("fill") boolean fill) {
        if (fill) {
            coffeeMachine.fillTank();
        }
    }

    @RequestMapping(value = "/tank", method = RequestMethod.PUT, params = "empty")
    public void emptyTank(@RequestParam("empty") boolean empty) {
        if (empty) {
            coffeeMachine.emptyTank();
        }
    }



}
