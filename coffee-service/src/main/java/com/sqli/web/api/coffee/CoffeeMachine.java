package com.sqli.web.api.coffee;

import org.springframework.stereotype.Component;

/**
 * Created by ema on 08/06/16.
 */
@Component
public class CoffeeMachine {

    boolean emptyTank;

    public void fillTank() {
        setEmptyTank(false);
    }

    public void emptyTank() {
        setEmptyTank(true);
    }

    public boolean isEmptyTank() {
        return emptyTank;
    }

    private void setEmptyTank(boolean emptyTank) {
        this.emptyTank = emptyTank;
    }
}
